package alkolib

import (
	"fmt"
	"net/url"
	"os"
	"sync"

	"bytes"
	"strconv"
	"time"

	"github.com/PuerkitoBio/goquery"
	raven "github.com/getsentry/raven-go"
	"github.com/inconshreveable/log15"
	"github.com/spf13/viper"
	"github.com/valyala/fasthttp"
)

// Inventory is number of products in stock in a store
type Inventory struct {
	ProductID   uint64
	TotalStock  uint32
	StoreStocks []storeStock
}

type storeStock struct {
	StoreID uint64
	Stock   uint32
}

var wg sync.WaitGroup

var productChan chan Product
var inventoryChan chan Inventory

var workersInitialized bool

// InitAvailabilityWorkerPool initializes the worker pool
func InitAvailabilityWorkerPool() (err error) {
	if workersInitialized {
		return nil
	}
	numWorkers := viper.GetInt("num_workers")
	productChan = make(chan Product)
	inventoryChan = make(chan Inventory)

	for w := 1; w <= numWorkers; w++ {
		go AvailabilityWorker(w, productChan, inventoryChan, &wg)
	}

	workersInitialized = true
	return err
}

// QueueAvailabilityCheck inserts product to productChan
func QueueAvailabilityCheck(prod Product) {
	wg.Add(1)
	productChan <- prod

}

// UpdateAllProductAvailabilities checks availability for every product
func UpdateAllProductAvailabilities() {

	InitAvailabilityWorkerPool()

	go func() {
		InitDBPool()
		log15.Info("InventoryReceived", "started", nil)
		for inv := range inventoryChan {
			log15.Debug("InventoryReceived", "product", inv.ProductID, "num stores", len(inv.StoreStocks))
			stock := make(map[uint64]uint32)

			stockStores := make([]int32, len(inv.StoreStocks))

			for i, ss := range inv.StoreStocks {
				stock[ss.StoreID] = ss.Stock
				stockStores[i] = int32(ss.StoreID)
			}
			_, err := DBPool.Exec("INSERT INTO inventory (product_id,stock,total,updated) VALUES ($1,$2,$3,current_timestamp)", inv.ProductID, stock, inv.TotalStock)

			if err != nil {
				raven.CaptureErrorAndWait(err, nil)
				panic(err)
			}

			_, err = DBPool.Exec("UPDATE products SET availability_checked=current_timestamp,total_availability=$3,stores_available=$1 WHERE product_id = $2", stockStores, inv.ProductID, inv.TotalStock)

			if err != nil {
				raven.CaptureErrorAndWait(err, nil)
				panic(err)
			}

		}
	}()

	st := time.Now()
	numProducts := len(Products)
	targetTime := (time.Duration(time.Minute) * 50).Seconds()
	numHandled := 0
	for _, prod := range Products {
		timeFromStart := time.Now().Sub(st).Seconds()
		sleepTime := (targetTime - timeFromStart) / float64(numProducts-numHandled)

		log15.Debug("QueueAvailabilityCheck start", "num", numProducts, "handled", numHandled, "target", targetTime, "fromstart", timeFromStart, "product", prod.ID, "sleeptime", sleepTime)
		QueueAvailabilityCheck(prod)
		time.Sleep(time.Duration(sleepTime*1000) * time.Millisecond)
		numHandled++
	}

	os.Exit(1)
	wg.Wait()
	log15.Info("UpdateAllProductAvailabilities", "stage", "done", "took", time.Now().Sub(st))
}

// AvailabilityWorker go routing for availability checking
func AvailabilityWorker(workerID int, productChan <-chan Product, inventoryChan chan<- Inventory, wgp *sync.WaitGroup) {
	log15.Info("AvailabilityWorker", "stage", "started", "id", workerID)

	for prod := range productChan {
		log15.Debug("AvailabilityWorker", "stage", "work", "worker", workerID, "product", prod.ID)

		invs, err := GetProductAvailability(prod)

		if err == nil {
			inventoryChan <- invs
		}

		(*wgp).Done()
	}
}

// GetProductAvailability gets stores where product is available
func GetProductAvailability(product Product) (Inventory, error) {

	produrl := fmt.Sprintf("https://www.alko.fi/INTERSHOP/web/WFS/Alko-OnlineShop-Site/fi_FI/-/EUR/ViewProduct-Include?SKU=%06d&AjaxRequestMarker=true", product.ID)

	inv := Inventory{}
	inv.ProductID = product.ID

	log15.Debug("GetProductAvailability", "SKU", product.ID, "url", produrl)

	r := fasthttp.AcquireRequest()
	r.Header.SetUserAgent("Mozilla/5.0 (Linux; U; Android 3.0; en-us; Xoom Build/HRI39) AppleWebKit/534.13 (KHTML, like Gecko) Version/4.0 Safari/534.13")
	r.Header.SetMethod("GET")
	r.SetRequestURI(produrl)

	defer fasthttp.ReleaseRequest(r)

	res := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(res)

	if err := fasthttp.DoTimeout(r, res, time.Duration(60)*time.Second); err != nil {
		log15.Error(err.Error(), "SKU", product.ID)
		raven.CaptureError(err, map[string]string{"SKU": strconv.FormatUint(product.ID, 10)})
		return inv, err
	}

	body := res.Body()

	res.ReleaseBody(50)

	br := bytes.NewReader(body)

	doc, err := goquery.NewDocumentFromReader(br)
	if err != nil {
		log15.Error(err.Error(), "SKU", product.ID)
		raven.CaptureError(err, map[string]string{"SKU": strconv.FormatUint(product.ID, 10)})
		return inv, err
	}

	var totalstock uint32
	totalstock = 0
	doc.Find("a.store-item-link").Each(func(i int, s *goquery.Selection) {
		dataurl, exists := s.Attr("data-url")
		if !exists {
			raven.CaptureMessage("GetProductAvailability data-url not found", map[string]string{"SKU": strconv.FormatUint(product.ID, 10)})
			log15.Debug("GetProductAvailability not available", "SKU", product.ID)
			return
		}
		u, err := url.Parse(dataurl)
		if err != nil {
			log15.Error(err.Error(), "SKU", product.ID)
			raven.CaptureError(err, map[string]string{"SKU": strconv.FormatUint(product.ID, 10)})
			return
		}
		m, _ := url.ParseQuery(u.RawQuery)
		//

		ss := storeStock{}
		ss.StoreID, err = strconv.ParseUint(m["StoreID"][0], 10, 64)
		stock, err := strconv.ParseUint(m["StoreStock"][0], 10, 32)
		ss.Stock = uint32(stock)
		totalstock += uint32(stock)

		inv.StoreStocks = append(inv.StoreStocks, ss)
	})

	inv.TotalStock = totalstock

	log15.Debug("GetProductAvailability", "SKU", product.ID, "stores", len(inv.StoreStocks), "stock", totalstock)

	return inv, err

}
