package alkolib

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"text/tabwriter"
	"time"

	"math"

	"github.com/davecgh/go-spew/spew"
	raven "github.com/getsentry/raven-go"
	"github.com/inconshreveable/log15"
	"github.com/mdlayher/untappd"
	"github.com/spf13/viper"
)

func doUntappdRequest(endpoint string, params ...interface{}) ([]byte, error) {
	url := fmt.Sprintf("https://api.untappd.com%s?client_id=%s&client_secret=%s", fmt.Sprintf(endpoint, params...), viper.GetString("untappd_id"), viper.GetString("untappd_secret"))

	body, err := getURL(url)

	if err != nil {
		panic(err)
	}

	return body, err

}

// tabWriter returns a *tabwriter.Writer appropriately configured
// for tabular output.
func tabWriter() *tabwriter.Writer {
	return tabwriter.NewWriter(os.Stdout, 0, 8, 2, '\t', 0)
}

func printRateLimit(res *http.Response) {
	const header = "X-Ratelimit-Remaining"
	if v := res.Header.Get(header); v != "" {
		log.Printf("%s: %s", header, v)
	}
}

func getRateLimitRemaining(res *http.Response) (remaining int, err error) {
	const header = "X-Ratelimit-Remaining"
	if v := res.Header.Get(header); v != "" {
		remaining, err = strconv.Atoi(v)
	}

	return remaining, err
}

func printBeers(beers []*untappd.Beer) {
	tw := tabWriter()

	// Print field header
	fmt.Fprintln(tw, "ID\tName\tBrewery\tStyle\tABV\tIBU")

	// Print out each beer
	for _, b := range beers {
		fmt.Fprintf(tw, "%d\t%s\t%s\t%s\t%0.1f\t%03d\n",
			b.ID,
			b.Name,
			b.Brewery.Name,
			b.Style,
			b.ABV,
			b.IBU,
		)
	}

	// Flush buffered output
	if err := tw.Flush(); err != nil {
		log.Fatal(err)
	}
}

// FindPossibleUntappdMatches finds possible matches for beers in Products from Untappd
func FindPossibleUntappdMatches() (err error) {
	log15.Debug("FindPossibleUntappdMatches")

	InitDBPool()

	uc, err := untappd.NewClient(viper.GetString("untappd_id"), viper.GetString("untappd_secret"), nil)
	if err != nil {
		panic(err)
	}

	i := 0

	remainingTarget := time.Now().Add(time.Duration(1) * time.Hour)
	lastRemaining := 0

	type UntappdAlkoMatch struct {
		AlkoID    uint64
		UntappdID int32
		Possible  []int32
		Checked   time.Time
	}
	matches := make(map[uint64]UntappdAlkoMatch)

	ct, err := DBPool.Exec("INSERT INTO untappd_alko_beers (alko_id) (SELECT product_id FROM products WHERE type = 'oluet' AND product_id NOT IN (SELECT alko_id FROM untappd_alko_beers));")

	if err != nil {
		raven.CaptureError(err, map[string]string{"step": "INSERT untappd_alko_beers"})
		log15.Crit("FindPossibleUntappdMatches", err.Error(), "step", "INSERT untappd_alko_beers")
		return err
	}

	log15.Info("FindPossibleUntappdMatches", "step", "INSERT", "tag", ct)

	rows, err := DBPool.Query("SELECT alko_id,untappd_id FROM untappd_alko_beers WHERE untappd_id IS NULL")

	if err != nil {
		raven.CaptureError(err, map[string]string{"step": "SELECT untappd_alko_beers"})
		log15.Crit("FindPossibleUntappdMatches", err.Error(), "step", "SELECT untappd_alko_beers")
		return err
	}

	defer rows.Close()

	for rows.Next() {
		euam := new(UntappdAlkoMatch)
		err = rows.Scan(&euam.AlkoID, &euam.UntappdID)
		if err != nil {
			raven.CaptureError(err, map[string]string{"step": "Scan"})
			log15.Crit("FindPossibleUntappdMatches", err.Error(), "step", "Scan")
			return err
		}
		matches[euam.AlkoID] = *euam
	}

	for _, prod := range Products {
		if prod.Type != "oluet" {
			continue
		}

		if _, exists := matches[prod.ID]; exists {
			continue
		}
		log15.Debug("FindPossibleUntappdMatches", "i", i, "SKU", prod.ID, "EBU", prod.EBU, "ABV", prod.Alcohol, "name", prod.Name)

		beers, res, err := uc.Beer.Search(prod.Name)

		if err != nil {
			raven.CaptureErrorAndWait(err, nil)
			panic(err)
		}

		remaining, err := getRateLimitRemaining(res)
		secondsToTarget := remainingTarget.Sub(time.Now()).Seconds() + 60
		if remaining > lastRemaining {
			remainingTarget = time.Now().Add(time.Duration(1) * time.Hour)
		}
		lastRemaining = remaining

		var sleepTime float64
		sleepTime = secondsToTarget / float64(remaining)
		if remaining < 7 && secondsToTarget < 120 {

			sleepTime = (time.Duration(10) * time.Minute).Seconds()
		}

		log15.Debug("FindPossibleUntappdMatches", "i", i, "SKU", prod.ID, "num results", len(beers), "remaining", remaining, "secondsToTarget", secondsToTarget, "sleepTime", sleepTime)

		uam := UntappdAlkoMatch{}
		uam.AlkoID = prod.ID
		uam.Checked = time.Now()
		uam.Possible = make([]int32, len(beers))
		printBeers(beers)
		alcbeers := beers[:0]
		for i, beer := range beers {
			_, err := DBPool.Exec("INSERT INTO untappd_beers (beer_id,data,added,updated) VALUES ($1,$2,current_timestamp,current_timestamp) ON CONFLICT (beer_id) DO UPDATE SET updated=current_timestamp,data = EXCLUDED.data;", beer.ID, beer)

			if err != nil {
				spew.Dump(beer)
				raven.CaptureError(err, map[string]string{"beer": strconv.Itoa(beer.ID)})
				log15.Error(err.Error())
			}
			uam.Possible[i] = int32(beer.ID)
			if math.Abs(float64(prod.Alcohol)-beer.ABV) < 0.2 {
				alcbeers = append(alcbeers, beer)
			}
		}

		printBeers(alcbeers)
		if len(alcbeers) == 1 {
			uam.UntappdID = int32(alcbeers[0].ID)
		}

		_, err = DBPool.Exec("INSERT INTO untappd_alko_beers (alko_id,untappd_id,possible,checked) VALUES ($1,$2,$3,current_timestamp)", uam.AlkoID, uam.UntappdID, uam.Possible)
		if err != nil {
			spew.Dump(uam)
			raven.CaptureError(err, map[string]string{"SKU": strconv.FormatUint(prod.ID, 10)})
			log15.Error(err.Error())
		}

		time.Sleep(time.Duration(sleepTime) * time.Second)
		i++

	}
	return err

}

// UpdateUntappdBeerInfo updates info from untappd on matched beers
func UpdateUntappdBeerInfo() (err error) {
	InitDBPool()

	uc, err := untappd.NewClient(viper.GetString("untappd_id"), viper.GetString("untappd_secret"), nil)
	if err != nil {
		raven.CaptureErrorAndWait(err, nil)
		log15.Crit("UpdateUntappdBeerInfo", err.Error(), "step", "SELECT untappd_alko_beers")
		panic(err)
	}

	rows, err := DBPool.Query("SELECT DISTINCT uab.untappd_id,ub.updated FROM untappd_alko_beers uab LEFT JOIN untappd_beers ub ON uab.untappd_id=ub.beer_id WHERE untappd_id IS NOT NULL AND untappd_id > 0 ORDER BY ub.updated ASC NULLS FIRST")

	if err != nil {
		raven.CaptureError(err, map[string]string{"step": "SELECT untappd_alko_beers"})
		log15.Crit("UpdateUntappdBeerInfo", err.Error(), "step", "SELECT untappd_alko_beers", nil)
		return err
	}

	defer rows.Close()

	var untappdID int32
	var untappdUpdated time.Time

	untappdBeers := make([]int32, 1000)
	i := 0

	for rows.Next() {

		err = rows.Scan(&untappdID, &untappdUpdated)
		if err != nil {
			raven.CaptureError(err, map[string]string{"step": "Scan"})
			log15.Crit("UpdateUntappdBeerInfo", err.Error(), "step", "Scan")
			return err
		}

		untappdBeers[i] = untappdID
		i++
	}

	if rows.Err() != nil {
		raven.CaptureError(err, map[string]string{"step": "PostScane"})
		log15.Crit("UpdateUntappdBeerInfo", err.Error(), "step", "PostScan")
		return err
	}

	untappdBeers = untappdBeers[:i]

	remainingTarget := time.Now().Add(time.Duration(1) * time.Hour)
	lastRemaining := 0

	for i, uid := range untappdBeers {

		beer, res, err := uc.Beer.Info(int(uid), true)

		if err != nil {
			raven.CaptureErrorAndWait(err, map[string]string{"beer_id": strconv.Itoa(int(uid))})
			panic(err)
		}

		_, err = DBPool.Exec("INSERT INTO untappd_beers (beer_id,data,added,updated) VALUES ($1,$2,current_timestamp,current_timestamp) ON CONFLICT (beer_id) DO UPDATE SET updated=current_timestamp,data = EXCLUDED.data;", beer.ID, beer)

		if err != nil {
			spew.Dump(beer)
			raven.CaptureError(err, map[string]string{"beer": strconv.Itoa(beer.ID)})
			log15.Error(err.Error())
		}

		remaining, err := getRateLimitRemaining(res)
		secondsToTarget := remainingTarget.Sub(time.Now()).Seconds() + 60
		if remaining > lastRemaining {
			remainingTarget = time.Now().Add(time.Duration(1) * time.Hour)
		}
		lastRemaining = remaining

		var sleepTime float64
		sleepTime = secondsToTarget / float64(remaining)
		if remaining < 7 && secondsToTarget < 120 {

			sleepTime = (time.Duration(10) * time.Minute).Seconds()
		}

		log15.Debug("UpdateUntappdBeerInfo", "i", i, "beer", beer.ID, "remaining", remaining, "secondsToTarget", secondsToTarget, "sleepTime", sleepTime)

		time.Sleep(time.Duration(sleepTime) * time.Second)
	}

	return err
}
