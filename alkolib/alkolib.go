package alkolib

import (
	"fmt"
	"time"

	raven "github.com/getsentry/raven-go"
	"github.com/inconshreveable/log15"
	"github.com/jackc/pgx"
	"github.com/spf13/viper"
	"github.com/valyala/fasthttp"
)

// DBPool is the DB connection pool
var DBPool *pgx.ConnPool
var dbPoolInitialized bool

// InitAlkolib inits Alkolib
func InitAlkolib() (err error) {

	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.AddConfigPath("/home/teemu/gosrc/alko-backend")

	err = viper.ReadInConfig() // Find and read the config file

	raven.SetDSN(viper.GetString("sentry_dsn"))

	if err != nil { // Handle errors reading the config file
		raven.CaptureErrorAndWait(err, nil)
		panic(fmt.Errorf("fatal error config file: %s", err))
	}

	return err
}

//InitDBPool initializes the DB connection pool
func InitDBPool() error {
	if dbPoolInitialized {
		return nil
	}
	var err error

	dbhost := viper.GetString("db.host")
	dbport := uint16(viper.GetInt("db.port"))
	dbdbname := viper.GetString("db.dbname")
	dbuser := viper.GetString("db.user")
	dbpassword := viper.GetString("db.password")

	DBPool, err = pgx.NewConnPool(pgx.ConnPoolConfig{
		ConnConfig: pgx.ConnConfig{
			Host:     dbhost,
			Port:     dbport,
			Database: dbdbname,
			User:     dbuser,
			Password: dbpassword},
		MaxConnections: 25})

	if err != nil {
		log15.Crit("InitDBPool", err.Error())
		raven.CaptureErrorAndWait(err, nil)
		panic(err)
		return err
	}

	log15.Info("InitDBPool", "status", "initialized")
	dbPoolInitialized = true

	return err
}

func getURL(url string) ([]byte, error) {
	r := fasthttp.AcquireRequest()
	r.Header.SetUserAgent("Mozilla/5.0 (Linux; U; Android 3.0; en-us; Xoom Build/HRI39) AppleWebKit/534.13 (KHTML, like Gecko) Version/4.0 Safari/534.13")
	r.Header.SetMethod("GET")
	r.SetRequestURI(url)

	defer fasthttp.ReleaseRequest(r)

	res := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(res)

	if err := fasthttp.DoTimeout(r, res, time.Duration(30)*time.Second); err != nil {
		log15.Crit(err.Error())
		raven.CaptureErrorAndWait(err, nil)
		return nil, err
	}

	body := res.Body()

	res.ReleaseBody(50)

	return body, nil
}
