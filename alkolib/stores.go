package alkolib

import (
	"bytes"
	"strconv"

	"github.com/PuerkitoBio/goquery"
	"github.com/buger/jsonparser"
	raven "github.com/getsentry/raven-go"
	"github.com/inconshreveable/log15"
)

// Store type
type Store struct {
	ID         uint64
	OutletType uint8
	Name       string
	Lat        float64
	Lon        float64
	Phone      string
	Address    string
	PostalCode string
	City       string
	OpenDay0   string
	OpenDay1   string
}

/*
CREATE TABLE stores (
	store_id integer PRiMARY KEY,
	type int,
	name text,
	lat double precision,
	lon double precision,
	phone text,
	address text,
	postalcode text,
	city text,
	openday0 text,
	openday1 text,
	spotted timestamp with time zone,
	updated timestamp with time zone
);

CREATE TABLE inventory (
	product_id int,
	store_id int,
	availability int,
	updated timestamp with time zone
);
CREATE INDEX idx_inventory_product_id ON inventory (product_id);
CREATE INDEX idx_inventory_store_id ON inventory (store_id);
CREATE INDEX idx_inventory_updated ON inventory (updated);

*/

// RetrieveStoreInfo retrieves store info from ALKO site
func RetrieveStoreInfo() {

	storeurl := "https://www.alko.fi/myymalat-palvelut/palvelut/"
	body, err := getURL(storeurl)

	if err != nil {
		panic(err)
	}

	br := bytes.NewReader(body)

	doc, err := goquery.NewDocumentFromReader(br)
	if err != nil {
		log15.Crit(err.Error())
		raven.CaptureErrorAndWait(err, nil)
		return
	}

	var jsontext string

	doc.Find("script").Each(func(i int, s *goquery.Selection) {
		_, ex2 := s.Attr("data-store-data")
		if !ex2 {
			return
		}

		jsontext = s.Text()
	})

	if len(jsontext) == 0 {
		log15.Crit("Unable to retrieve store JSON")
		raven.CaptureMessage("Unable to retrieve store JSON", nil)
		return
	}

	jsondata := []byte(jsontext)

	if err != nil {
		raven.CaptureErrorAndWait(err, nil)
		log15.Crit(err.Error())
		return
	}

	storekeys := [][]string{
		[]string{"storeId"},
		[]string{"outletTypeId"},
		[]string{"name"},
		[]string{"latitude"},
		[]string{"longitude"},
		[]string{"address"},
		[]string{"postalCode"},
		[]string{"city"},
		[]string{"OpenDay0"},
		[]string{"OpenDay1"},
		[]string{"phone"},
	}

	myymalat := []byte("myymalat")
	tilaus := []byte("tilauspalvelupisteet")
	stores := make([]Store, 1000)
	i := 0
	_, err = jsonparser.ArrayEach(jsondata, func(storevalue []byte, dataType jsonparser.ValueType, offset int, err error) {
		store := Store{}
		jsonparser.EachKey(storevalue, func(idx int, value []byte, vt jsonparser.ValueType, err error) {

			switch idx {
			case 0:

				store.ID, err = strconv.ParseUint(string(value[:len(value)]), 10, 64)
				if err != nil {
					raven.CaptureErrorAndWait(err, map[string]string{"ID field": string(value[:len(value)])})
					log15.Crit(err.Error(), "ID field", string(value[:len(value)]))
					return
				}
			case 1:

				if bytes.HasSuffix(value, myymalat) {
					store.OutletType = 1
				} else if bytes.HasSuffix(value, tilaus) {
					store.OutletType = 2
				}

			case 2:
				store.Name = string(value[:len(value)])
			case 3:
				store.Lat, err = strconv.ParseFloat(string(value[:len(value)]), 64)
			case 4:
				store.Lon, err = strconv.ParseFloat(string(value[:len(value)]), 64)
			case 5:
				store.Address = string(value[:len(value)])
			case 6:
				store.PostalCode = string(value[:len(value)])
			case 7:
				store.City = string(value[:len(value)])
			case 8:
				store.OpenDay0 = string(value[:len(value)])
			case 9:
				store.OpenDay1 = string(value[:len(value)])
			case 10:
				store.Phone = string(value[:len(value)])
			}
		}, storekeys...)

		stores[i] = store
		i++
	}, "stores")
	stores = stores[:i]

	if err != nil {
		raven.CaptureErrorAndWait(err, nil)
		log15.Crit(err.Error())
		return
	}

	InitDBPool()

	for _, s := range stores {

		_, err := DBPool.Exec("INSERT INTO stores (store_id,type,name,lat,lon,phone,address,postalcode,city,openday0,openday1,spotted) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,current_timestamp) ON CONFLICT (store_id) DO UPDATE set type=$2,name=$3,lat=$4,lon=$5,phone=$6,address=$7,postalcode=$8,city=$9,openday0=$10,openday1=$11,updated=current_timestamp", s.ID, s.OutletType, s.Name, s.Lat, s.Lon, s.Phone, s.Address, s.PostalCode, s.City, s.OpenDay0, s.OpenDay1)
		if err != nil {
			raven.CaptureErrorAndWait(err, nil)
			log15.Crit(err.Error())
			continue
		}

	}

	log15.Info("RetrieveStoreInfo", "state", "done", "num stores", len(stores))

}
