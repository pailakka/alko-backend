package alkolib

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"math"
	"os"
	"reflect"
	"time"

	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/davecgh/go-spew/spew"
	raven "github.com/getsentry/raven-go"
	"github.com/inconshreveable/log15"
	"github.com/shopspring/decimal"
)

// Product is a struct describing a single alko product
type Product struct {
	ID               uint64
	Name             string
	Producer         string
	BottleSize       float32
	Price            decimal.Decimal
	PricePerLiter    decimal.Decimal
	NewItem          bool
	ProductListOrder string
	Type             string
	SpecialGroup     string
	BeerType         string
	ProductCountry   string
	ProductArea      string
	ProductYear      int
	LabelMarkings    string
	Notes            string
	Grapes           []string
	Description      []string
	Package          string
	Top              string
	Alcohol          float32
	Acid             float32
	Sugar            float32
	Gravity          float32
	EBC              float32
	EBU              float32
	Energy           uint64
	Selection        string
	Seen             bool
}

func strsEquals(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func (p1 *Product) equals(p2 Product) bool {

	debuglog := true

	if p1.ID != p2.ID {
		if debuglog {
			log15.Debug("equals", "mode", "ID", "p1", p1.ID, "p2", p2.ID)
		}
		return false
	}
	if p1.Name != p2.Name {
		if debuglog {
			log15.Debug("equals", "mode", "Name", "p1", p1.Name, "p2", p2.Name)
		}
		return false
	}
	if p1.Producer != p2.Producer {
		if debuglog {
			log15.Debug("equals", "mode", "Producer", "p1", p1.Producer, "p2", p2.Producer)
		}
		return false
	}
	if p1.BottleSize != p2.BottleSize {
		if debuglog {
			log15.Debug("equals", "mode", "BottleSize", "p1", p1.BottleSize, "p2", p2.BottleSize)
		}
		return false
	}
	pdiff, _ := p1.Price.Sub(p2.Price).Abs().Float64()
	if pdiff > 0.001 {
		if debuglog {
			log15.Debug("equals", "mode", "Price", "p1", p1.Price, "p2", p2.Price)
		}
		return false
	}
	ppldiff, _ := p1.PricePerLiter.Sub(p2.PricePerLiter).Abs().Float64()

	if ppldiff > 0.001 {
		if debuglog {
			log15.Debug("equals", "mode", "PricePerLiter", "p1", p1.PricePerLiter, "p2", p2.PricePerLiter)
		}
		return false
	}
	if p1.NewItem != p2.NewItem {
		if debuglog {
			log15.Debug("equals", "mode", "NewItem", "p1", p1.NewItem, "p2", p2.NewItem)
		}
		return false
	}
	if p1.ProductListOrder != p2.ProductListOrder {
		if debuglog {
			log15.Debug("equals", "mode", "ProductListOrder", "p1", p1.ProductListOrder, "p2", p2.ProductListOrder)
		}
		return false
	}
	if p1.Type != p2.Type {
		if debuglog {
			log15.Debug("equals", "mode", "Type", "p1", p1.Type, "p2", p2.Type)
		}
		return false
	}
	if p1.SpecialGroup != p2.SpecialGroup {
		if debuglog {
			log15.Debug("equals", "mode", "SpecialGroup", "p1", p1.SpecialGroup, "p2", p2.SpecialGroup)
		}
		return false
	}
	if p1.BeerType != p2.BeerType {
		if debuglog {
			log15.Debug("equals", "mode", "BeerType", "p1", p1.BeerType, "p2", p2.BeerType)
		}
		return false
	}
	if p1.ProductCountry != p2.ProductCountry {
		if debuglog {
			log15.Debug("equals", "mode", "ProductCountry", "p1", p1.ProductCountry, "p2", p2.ProductCountry)
		}
		return false
	}
	if p1.ProductArea != p2.ProductArea {
		if debuglog {
			log15.Debug("equals", "mode", "ProductArea", "p1", p1.ProductArea, "p2", p2.ProductArea)
		}
		return false
	}
	if p1.ProductYear != p2.ProductYear {
		if debuglog {
			log15.Debug("equals", "mode", "ProductYear", "p1", p1.ProductYear, "p2", p2.ProductYear)
		}
		return false
	}
	if p1.LabelMarkings != p2.LabelMarkings {
		if debuglog {
			log15.Debug("equals", "mode", "LabelMarkings", "p1", p1.LabelMarkings, "p2", p2.LabelMarkings)
		}
		return false
	}
	if p1.Notes != p2.Notes {
		if debuglog {
			log15.Debug("equals", "mode", "Notes", "p1", p1.Notes, "p2", p2.Notes)
		}
		return false
	}
	if !strsEquals(p1.Description, p2.Description) {
		if debuglog {
			log15.Debug("equals", "mode", "Description", "p1", p1.Description, "p2", p2.Description)
		}
		return false
	}

	if p1.Package != p2.Package {
		if debuglog {
			log15.Debug("equals", "mode", "Package", "p1", p1.Package, "p2", p2.Package)
		}
		return false
	}

	if p1.Top != p2.Top {
		if debuglog {
			log15.Debug("equals", "mode", "Top", "p1", p1.Top, "p2", p2.Top)
		}
		return false
	}

	if math.Abs(float64(p1.Alcohol-p2.Alcohol)) > 0.001 {
		if debuglog {
			log15.Debug("equals", "mode", "Alcohol", "p1", p1.Alcohol, "p2", p2.Alcohol)
		}
		return false
	}

	if math.Abs(float64(p1.Acid-p2.Acid)) > 0.001 {
		if debuglog {
			log15.Debug("equals", "mode", "Acid", "p1", p1.Acid, "p2", p2.Acid)
		}
		return false
	}
	if math.Abs(float64(p1.Sugar-p2.Sugar)) > 0.001 {
		if debuglog {
			log15.Debug("equals", "mode", "Sugar", "p1", p1.Sugar, "p2", p2.Sugar)
		}
		return false
	}

	if math.Abs(float64(p1.Gravity-p2.Gravity)) > 0.001 {
		if debuglog {
			log15.Debug("equals", "mode", "Gravity", "p1", p1.Gravity, "p2", p2.Gravity)
		}
		return false
	}
	if math.Abs(float64(p1.EBC-p2.EBC)) > 0.001 {
		if debuglog {
			log15.Debug("equals", "mode", "EBC", "p1", p1.EBC, "p2", p2.EBC)
		}
		return false
	}
	if math.Abs(float64(p1.EBU-p2.EBU)) > 0.001 {
		if debuglog {
			log15.Debug("equals", "mode", "EBU", "p1", p1.EBU, "p2", p2.EBU)
		}
		return false
	}
	if math.Abs(float64(p1.Energy-p2.Energy)) > 1.0 {
		if debuglog {
			log15.Debug("equals", "mode", "Energy", "p1", p1.Energy, "p2", p2.Energy)
		}
		return false
	}

	if p1.Selection != p2.Selection {
		if debuglog {
			log15.Debug("equals", "mode", "Selection", "p1", p1.Selection, "p2", p2.Selection)
		}
		return false
	}

	return true
}

/*
product_id integer PRIMARY KEY,
name text,
producer text,
bottlesize real,
price decimal,
priceperliter decimal,
newitem boolean,
productlistorder text,
type text,
specialgroup text,
beertype text,
productcountry text,
productarea text,
productyear integer,
labelmarkings text,
notes text,
grapes text[],
description text[],
package text,
top text,
alcohol real,
acid real,
sugar real,
gravity real,
ebc real,
ebu real,
energy integer,
selection text,
spotted timestamp with time zone,
updated timestamp with time zone,
total_availability integer,
availability_checked timestamp with time zone,
stores_available integer[]


*/

func trimStrings(sl []string, tolower bool) []string {
	rsl := make([]string, len(sl))

	for i, s := range sl {
		if tolower {
			s = strings.ToLower(s)
		}
		rsl[i] = strings.TrimSpace(s)
	}

	return rsl
}

func parseFloatToField(p interface{}, field, source string) {
	if len(source) == 0 {
		return
	}
	e := reflect.ValueOf(p).Elem()
	f := e.FieldByName(field)
	var fv float64

	fv, err := strconv.ParseFloat(strings.Replace(source, ",", ".", -1), 64)
	fv2 := float32(fv)

	if err != nil {
		if len(source) > 0 {
			log15.Crit(err.Error(), "ID", e.FieldByName("ID").String(), "field", field)
		}
	}

	f.Set(reflect.ValueOf(fv2))

}

// Products contains products in a map as number as key
var Products map[uint64]Product

// LoadProductsFromDB loads products from DB
func LoadProductsFromDB() (err error) {
	InitDBPool()
	Products = make(map[uint64]Product)

	st := time.Now()
	rows, err := DBPool.Query("SELECT product_id,name,producer,bottlesize,price,priceperliter,newitem,productlistorder,type,specialgroup,beertype,productcountry,productarea,productyear,labelmarkings,notes,grapes,description,package,top,alcohol,acid,sugar,gravity,ebc,ebu,energy,selection FROM products")

	if err != nil {
		raven.CaptureError(err, map[string]string{"step": "SELECT"})
		log15.Crit("LoadProductsFromDB", err.Error(), "step", "SELECT")
		return err
	}

	defer rows.Close()

	for rows.Next() {
		p := new(Product)
		p.Seen = false

		err = rows.Scan(&p.ID, &p.Name, &p.Producer, &p.BottleSize, &p.Price, &p.PricePerLiter, &p.NewItem, &p.ProductListOrder, &p.Type, &p.SpecialGroup, &p.BeerType, &p.ProductCountry, &p.ProductArea, &p.ProductYear, &p.LabelMarkings, &p.Notes, &p.Grapes, &p.Description, &p.Package, &p.Top, &p.Alcohol, &p.Acid, &p.Sugar, &p.Gravity, &p.EBC, &p.EBU, &p.Energy, &p.Selection)
		if err != nil {
			raven.CaptureError(err, map[string]string{"step": "Scan"})
			log15.Crit("LoadProductsFromDB", err.Error(), "step", "Scan")
			return err
		}
		Products[p.ID] = *p
	}

	log15.Info("LoadProductsFromDB", "took", time.Now().Sub(st), "products", len(Products))

	return err

}
func insertOrUpdateProduct(p Product) {
	InitDBPool()
	p2, exists := Products[p.ID]

	if exists {
		if !p.equals(p2) {
			log15.Debug("insertOrUpdateProduct", "SKU", p.ID, "status", "changed")

			_, err := DBPool.Exec("INSERT INTO product_changes (product_id,old,new,changed) VALUES ($1,$2,$3,current_timestamp)", p2.ID, p2, p)

			if err != nil {
				spew.Dump(p)
				raven.CaptureError(err, map[string]string{"SKU": strconv.FormatUint(p.ID, 10)})
				log15.Crit(err.Error())
				return
			}

			_, err = DBPool.Exec("UPDATE products SET name=$1, producer=$2, bottlesize=$3, price=$4, priceperliter=$5, newitem=$6, productlistorder=$7, type=$8, specialgroup=$9, beertype=$10, productcountry=$11, productarea=$12, productyear=$13, labelmarkings=$14, notes=$15, grapes=$16, description=$17, package=$18, top=$19, alcohol=$20, acid=$21, sugar=$22, gravity=$23, ebc=$24, ebu=$25, energy=$26, selection=$27, seen=$28 updated=current_timestamp WHERE product_id = $29", p.Name, p.Producer, p.BottleSize, p.Price, p.PricePerLiter, p.NewItem, p.ProductListOrder, p.Type, p.SpecialGroup, p.BeerType, p.ProductCountry, p.ProductArea, p.ProductYear, p.LabelMarkings, p.Notes, p.Grapes, p.Description, p.Package, p.Top, p.Alcohol, p.Acid, p.Sugar, p.Gravity, p.EBC, p.EBU, p.Energy, p.Selection, p.Seen, p.ID)
			if err != nil {
				spew.Dump(p)
				raven.CaptureError(err, map[string]string{"SKU": strconv.FormatUint(p.ID, 10)})
				log15.Crit(err.Error())
				return
			}
			Products[p.ID] = p
		}
	} else {
		_, err := DBPool.Exec("INSERT INTO products (product_id,name,producer,bottlesize,price,priceperliter,newitem,productlistorder,type,specialgroup,beertype,productcountry,productarea,productyear,labelmarkings,notes,grapes,description,package,top,alcohol,acid,sugar,gravity,ebc,ebu,energy,selection,seen,spotted) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,current_timestamp)", p.ID, p.Name, p.Producer, p.BottleSize, p.Price, p.PricePerLiter, p.NewItem, p.ProductListOrder, p.Type, p.SpecialGroup, p.BeerType, p.ProductCountry, p.ProductArea, p.ProductYear, p.LabelMarkings, p.Notes, p.Grapes, p.Description, p.Package, p.Top, p.Alcohol, p.Acid, p.Sugar, p.Gravity, p.EBC, p.EBU, p.Energy, p.Selection, p.Seen)
		if err != nil {
			spew.Dump(p)
			raven.CaptureError(err, map[string]string{"SKU": strconv.FormatUint(p.ID, 10)})
			log15.Crit(err.Error())
			return
		}
		log15.Debug("insertOrUpdateProduct", "SKU", p.ID, "status", "new")
		Products[p.ID] = p
	}

}

// ReadProductsFromOnlineTxt reads productlist (hinnasto) from alko website and populates Products map
func ReadProductsFromOnlineTxt() (err error) {
	log15.Info("ReadProductsFromOnlineTxt")

	st := time.Now()

	pricelisturl := "https://www.alko.fi/valikoimat-ja-hinnasto/hinnasto"
	body, err := getURL(pricelisturl)

	if err != nil {
		panic(err)
	}

	br := bytes.NewReader(body)

	doc, err := goquery.NewDocumentFromReader(br)
	if err != nil {
		log15.Crit(err.Error())
		raven.CaptureErrorAndWait(err, nil)
		return
	}

	var txturl string
	doc.Find("div.teaser-column").Each(func(i int, s *goquery.Selection) {
		heading := s.Find("a.teaser-heading")
		headtext := strings.TrimSpace(heading.Text())
		if headtext == "Alkon hinnasto txt-tiedostona" {
			txturl, _ = heading.Attr("href")
		}
	})

	if txturl == "" {
		return errors.New("Unable to locate inventory txt url")
	}

	txturl = fmt.Sprintf("https://www.alko.fi%s", txturl)

	txtbody, err := getURL(txturl)

	if err != nil {
		panic(err)
	}

	txtbr := bufio.NewReader(bytes.NewReader(txtbody))

	err = readProductlistFromReader(txtbr)
	log15.Info("ReadProductsFromOnlineTxt", "took", time.Now().Sub(st), "num products", len(Products))
	return err
}

func readProductlistFromReader(br *bufio.Reader) (err error) {
	st := time.Now()

	_, _, err = br.ReadLine()

	if err != nil {
		log15.Crit(err.Error())
		raven.CaptureErrorAndWait(err, nil)
		panic(err)
	}

	r := csv.NewReader(br)

	r.Comma = '\t'
	r.LazyQuotes = true
	i := 1
	for {
		pl, err := r.Read()

		if err == io.EOF {
			break
		}
		if i < 2 {
			i++
			continue
		}

		if err != nil {
			log15.Crit(err.Error(), "line", i)
			i++
			continue
		}

		//fmt.Println(pl)

		p := new(Product)

		p.ID, err = strconv.ParseUint(pl[0], 10, 64)
		if err != nil {
			raven.CaptureError(err, map[string]string{"ID field": pl[0]})
			log15.Crit(err.Error(), "ID field", pl[0])
			i++
			continue
		}

		p.Name = strings.TrimSpace(pl[1])
		p.Producer = strings.TrimSpace(pl[2])

		parseFloatToField(p, "BottleSize", pl[3])

		p.Price, err = decimal.NewFromString(strings.Replace(pl[4], ",", ".", -1))

		if err != nil {
			p.Price = decimal.NewFromFloat(-1)
			if len(pl[4]) > 0 {
				raven.CaptureError(err, map[string]string{"ID": strconv.FormatUint(p.ID, 10), "field": "Price"})
				log15.Crit(err.Error(), "ID", p.ID, "field", "Price")
			}
		}

		p.PricePerLiter, err = decimal.NewFromString(strings.Replace(pl[5], ",", ".", -1))

		if err != nil {
			p.PricePerLiter = decimal.NewFromFloat(-1)
			if len(pl[5]) > 0 {
				raven.CaptureError(err, map[string]string{"ID": strconv.FormatUint(p.ID, 10), "field": "PricePerLiter"})
				log15.Crit(err.Error(), "ID", p.ID, "field", "PricePerLiter")
			}
		}

		p.NewItem = pl[6] == "uutuus"
		p.ProductListOrder = strings.TrimSpace(pl[7])
		p.Type = strings.TrimSpace(pl[8])
		p.SpecialGroup = strings.TrimSpace(pl[9])
		p.BeerType = strings.TrimSpace(pl[10])
		p.ProductCountry = strings.TrimSpace(pl[11])
		p.ProductArea = strings.TrimSpace(pl[12])
		p.ProductYear, err = strconv.Atoi(pl[13])

		if err != nil {
			p.ProductYear = -1
			if len(pl[13]) > 0 {
				raven.CaptureError(err, map[string]string{"ID": strconv.FormatUint(p.ID, 10), "field": "ProductYear"})
				log15.Crit(err.Error(), "ID", p.ID, "field", "ProductYear")
			}
		}

		p.LabelMarkings = strings.TrimSpace(pl[14])
		p.Notes = strings.TrimSpace(pl[15])
		if len(pl[16]) > 0 {
			p.Grapes = trimStrings(strings.Split(strings.TrimSpace(pl[16]), ","), false)
		}

		if len(pl[17]) > 0 {
			p.Description = trimStrings(strings.Split(strings.TrimSpace(pl[17]), ","), true)
		}

		p.Package = strings.TrimSpace(pl[18])
		p.Top = strings.TrimSpace(pl[19])

		parseFloatToField(p, "Alcohol", pl[20])
		parseFloatToField(p, "Acid", pl[21])
		parseFloatToField(p, "Sugar", pl[22])
		parseFloatToField(p, "Gravity", pl[23])
		parseFloatToField(p, "EBC", pl[24])
		parseFloatToField(p, "EBU", pl[25])

		p.Energy, err = strconv.ParseUint(pl[26], 10, 64)
		if err != nil {
			p.Energy = 0
			if len(pl[16]) > 0 {
				raven.CaptureError(err, map[string]string{"ID": strconv.FormatUint(p.ID, 10), "field": "Energy"})
				log15.Crit(err.Error(), "ID", p.ID, "field", "Energy")
			}
		}

		p.Selection = strings.TrimSpace(pl[27])
		p.Seen = true
		insertOrUpdateProduct(*p)
		//Products[p.ID] = *p
		i++
	}

	log15.Debug("Productlist read done", "took", time.Now().Sub(st), "num products", i)

	return nil
}

// ReadProductsFromTxt reads productlist (hinnasto) from text file and populates Products map
func ReadProductsFromTxt(fn string) (err error) {

	log15.Debug("Reading productlist", "filename", fn)

	f, err := os.Open(fn)
	if err != nil {
		log15.Crit(err.Error())
		raven.CaptureErrorAndWait(err, nil)
		panic(err)
	}

	br := bufio.NewReader(f)

	err = readProductlistFromReader(br)
	return err
}
