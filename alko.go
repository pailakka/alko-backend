package main

import (
	"flag"

	"github.com/inconshreveable/log15"

	"./alkolib"
)

func main() {

	modePtr := flag.String("mode", "update_alko", "Operating mode (update_alko/update_untappd/match_untappd)")

	flag.Parse()

	alkolib.InitAlkolib()

	switch *modePtr {
	case "update_alko":
		alkolib.LoadProductsFromDB()
		alkolib.ReadProductsFromOnlineTxt()
		alkolib.RetrieveStoreInfo()
		alkolib.UpdateAllProductAvailabilities()
	case "match_untappd":
		alkolib.LoadProductsFromDB()
		alkolib.ReadProductsFromOnlineTxt()
		alkolib.FindPossibleUntappdMatches()
	case "update_untappd":
		alkolib.LoadProductsFromDB()
		alkolib.ReadProductsFromOnlineTxt()
		alkolib.UpdateUntappdBeerInfo()
	default:
		log15.Crit("Unknown operating mode")
	}

}
